# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2016/01/26 17:36:14 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = fillit

SRC_PATH = ./src/
SRC_NAME = fillit.c ft_check_pieces.c ft_display_map.c ft_error.c ft_getfile.c\
		   ft_lib_pile.c ft_minilib.c ft_piececmp.c ft_resolve.c ft_move_piece.c

OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./includes/

CFLAGS = -Wall -Werror -Wextra

LIB_PATH = ./libft/

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))
LIB = $(addprefix -L,$(LIB_PATH))

LIB_NAME = -lft
LDFLAGS = $(LIB) $(LIB_NAME)


all: $(NAME)

$(NAME): $(OBJ)
	make -C $(LIB_PATH)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	@rm -f $(OBJ)
	@rmdir $(OBJ_PATH) 2>/dev/null || echo "" > /dev/null

fclean: clean
	@rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
