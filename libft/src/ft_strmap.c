/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:03:28 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 15:56:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(const char *s, char (*f)(char))
{
	char	*str;
	int		i;

	i = 0;
	if ((str = ft_strnew(ft_strlen(s) + 1)))
	{
		while (s[i])
		{
			str[i] = (*f)(s[i]);
			i++;
		}
	}
	return (str);
}
