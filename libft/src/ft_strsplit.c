/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:04:25 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/07 16:11:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_strlen_c(const char *s, char c)
{
	int		i;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	return (i);
}

static char		*ft_strcpy_to(char *dest, const char *src, char c)
{
	int		i;

	i = 0;
	while (src[i] && src[i] != c)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = 0;
	return (dest);
}

static void		ft_move_to(const char **s, char c, int *j)
{
	int		i;

	i = *j;
	while ((*s)[i] && (*s)[i] != c)
		i++;
	while ((*s)[i] && (*s)[i] == c)
		i++;
	*j = i;
}

static int		ft_count_words(const char *s, char c)
{
	int		i;
	int		b;

	i = 0;
	b = 0;
	while (*s)
	{
		if (!b && *s != c)
		{
			b = 1;
			i++;
		}
		b = (*s != c);
		s++;
	}
	return (i);
}

char			**ft_strsplit(const char *s, char c)
{
	int		nb_words;
	char	**tab;
	int		i;
	int		j;

	i = 0;
	j = 0;
	nb_words = ft_count_words(s, c);
	if (!(tab = malloc(sizeof(char*) * (nb_words + 1))))
		return (NULL);
	while (s[j] && s[j] == c)
		j++;
	while (s[j])
	{
		if (!(tab[i] = malloc(sizeof(char) * (ft_strlen_c(&(s[j]), c) + 1))))
			return (tab);
		ft_strcpy_to(tab[i], (s + j), c);
		ft_move_to(&s, c, &j);
		i++;
	}
	tab[i] = 0;
	return (tab);
}
