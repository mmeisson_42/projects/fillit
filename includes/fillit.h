/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 18:10:14 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 17:36:03 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "libft.h"
#include <stdio.h>

typedef struct		s_coord
{
	size_t		x;
	size_t		y;
}					t_coord;

typedef struct		s_piece
{
	char		**piece;
	t_coord		coord;
}					t_piece;

typedef struct		s_fifo
{
	t_piece			*piece;
	char			letter;
	struct s_fifo	*next;
}					t_fifo;

void			ft_getfile(char *file, t_fifo **actions);
void			ft_check_piece(char *buffer);
char			**ft_resolve(t_fifo *fifo, size_t *size);
void			ft_display_map(char **map, size_t size);

t_fifo			*ft_fifo_cmp(t_fifo *fifo, char *buffer);
int				ft_piece_cmp(char **tab, char *buffer);
char			**ft_init_map(size_t size);
int				ft_sqrt(int nb);
size_t			ft_fifolen(t_fifo *fifo);
t_piece			*ft_piece_new(char *buffer);
void			ft_manage_swap(t_fifo **fifo, char *buffer);
void			ft_push_fifo(t_fifo **head_fifo, t_piece *piece);
void			ft_breakfifo(t_fifo **fifo);
void			ft_manage_list(t_fifo **fifo, char *buffer);
void			ft_update_coord(t_coord *dst, t_coord *src);
void			ft_move_piece(char **piece);

void			ft_exit(void);
void			ft_error(void);

#endif
