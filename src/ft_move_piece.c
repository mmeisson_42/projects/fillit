/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_piece.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 15:06:58 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 15:24:24 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_move_to_top(char **piece)
{
	char		*tmp;
	size_t		i;

	i = 0;
	tmp = piece[i];
	while (i < 3)
	{
		piece[i] = piece[i + 1];
		i++;
	}
	piece[i] = tmp;
}

void		ft_swap_c(char *a, char *b)
{
	char	c;

	c = *a;
	*a = *b;
	*b = c;
}

void		ft_move_to_left(char **piece)
{
	size_t		i;
	size_t		j;

	j = 0;
	while (j < 4)
	{
		i = 0;
		while (i < 3)
		{
			ft_swap_c(&piece[j][i], &piece[j][i + 1]);
			i++;
		}
		j++;
	}
}

void		ft_move_piece(char **piece)
{
	while (!ft_strchr(piece[0], '#'))
		ft_move_to_top(piece);
	while (piece[0][0] != '#' && piece[1][0] != '#' &&
			piece[2][0] != '#' && piece[3][0] != '#')
		ft_move_to_left(piece);
}
