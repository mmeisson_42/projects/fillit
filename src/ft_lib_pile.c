/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lib_pile.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 18:26:26 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 18:04:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void			ft_update_coord(t_coord *dst, t_coord *src)
{
	dst->x = src->x;
	dst->y = src->y;
}

t_piece			*ft_piece_new(char *buffer)
{
	t_piece		*piece;

	if (!(piece = ft_memalloc(sizeof(*piece))))
		ft_exit();
	piece->piece = ft_strsplit(buffer, '\n');
	return (piece);
}

void		ft_del_piece(t_piece *piece)
{
	char		**tab;
	size_t		i;

	i = 0;
	tab = piece->piece;
	while (i < 4)
		free(tab[i++]);
	free(tab);
	free(piece);
}

void		ft_break_fifo(t_fifo **fifo)
{
	if (*fifo)
	{
		ft_break_fifo(&(*fifo)->next);
		ft_del_piece((*fifo)->piece);
		free(*fifo);
		fifo = NULL;
	}
}

void			ft_manage_list(t_fifo **fifo, char *buffer)
{
	t_fifo		*tmp;

	tmp = ft_fifo_cmp(*fifo, buffer);
	if (tmp)
		ft_push_fifo(fifo, tmp->piece);
	else
		ft_push_fifo(fifo, ft_piece_new(buffer));
}

void			ft_push_fifo(t_fifo **head_fifo, t_piece *piece)
{
	static t_fifo	*foot = NULL;
	static char		letter = 'A';
	t_fifo			*node;

	if (!(node = ft_memalloc(sizeof(*node))))
		ft_exit();
	node->piece = piece;
	node->letter = letter++;
	if (!*head_fifo)
	{
		*head_fifo = node;
		foot = node;
	}
	else
	{
		foot->next = node;
		foot = foot->next;
	}
}
