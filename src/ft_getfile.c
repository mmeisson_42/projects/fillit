/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getfile.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 18:15:59 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 14:47:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_getfile(char *file, t_fifo **actions)
{
	int		file_d;
	char	buffer[22];
	int		ret;
	int		last_ret;

	file_d = open(file, O_RDONLY);
	if (file_d == -1)
		ft_error();
	while ((ret = read(file_d, buffer, 21)) > 0)
	{
		buffer[ret] = '\0';
		ft_check_piece(buffer);
		ft_manage_list(actions, buffer);
		last_ret = ret;
	}
	if (last_ret != 20)
		ft_error();
	close(file_d);
}
