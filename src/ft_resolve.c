/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_resolve.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 13:54:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 17:46:13 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		ft_del_piece(char **map, char **piece, t_coord *coord)
{
	size_t		x;
	size_t		y;
	size_t		j;

	y = 0;
	j = 0;
	while (y < 4)
	{
		x = 0;
		while (x < 4)
		{
			if (piece[y][x] == '#')
			{
				map[coord->y + y][coord->x + x] = '.';
				if (++j == 4)
					return ;
			}
			++x;
		}
		++y;
	}
}

static void		ft_put_piece(char **map, t_fifo *fifo, t_coord *coord)
{
	size_t		x;
	size_t		y;
	size_t		j;
	char		**tab;

	tab = fifo->piece->piece;
	y = 0;
	j = 0;
	while (y < 4)
	{
		x = 0;
		while (x < 4)
		{
			if (tab[y][x] == '#')
			{
				map[coord->y + y][coord->x + x] = fifo->letter;
				if (++j == 4)
					return ;
			}
			++x;
		}
		++y;
	}
}

static int		ft_push_piece(char **map, t_fifo *fifo, t_coord *coord, size_t size)
{
	t_coord		curr;
	t_coord		tmp;
	size_t		j;
	char		**piece;

	curr.y = 0;
	j = 0;
	piece = fifo->piece->piece;
	while (curr.y < 4)
	{
		curr.x = 0;
		while (curr.x < 4)
		{
			if (piece[curr.y][curr.x] == '#')
			{
				tmp.x = coord->x + curr.x;
				tmp.y = coord->y + curr.y;
				if (tmp.y >= size || tmp.x >= size || map[tmp.y][tmp.x] != '.')
					return (1);
				++j;
				if (j == 4)
				{
					ft_put_piece(map, fifo, coord);
					return (0);
				}
			}
			++(curr.x);
		}
		++(curr.y);
	}
	return (1);
}

static int		ft_backtrack(t_fifo *fifo, char **map, size_t size)
{
	t_piece		*piece;
	t_coord		pos;
	t_coord		beg;

	if (!fifo)
		return (1);
	piece = fifo->piece;
	ft_update_coord(&pos, &(piece->coord));
	ft_update_coord(&beg, &pos);
	while (pos.y < size)
	{
		while (pos.x < size)
		{
			if (ft_push_piece(map, fifo, &pos, size) == 0)
			{
				ft_update_coord(&(piece->coord), &pos);
				if (ft_backtrack(fifo->next, map, size) == 1)
					return (1);
				ft_del_piece(map, piece->piece, &pos);
			}
			++pos.x;
		}
		pos.x = 0;
		++pos.y;
	}
	ft_update_coord(&(piece->coord), &beg);
	return (0);
}

void			ft_print_fifo(t_fifo *fifo)
{
	size_t		i;
	char		**str;

	while (fifo)
	{
		i = 0;
		printf("addr = %p\n", fifo->piece);
		str = fifo->piece->piece;
		while (i < 3)
			ft_putendl(str[i++]);
		ft_putchar('\n');
		fifo = fifo->next;
	}
	exit(1);
}

char			**ft_resolve(t_fifo *fifo, size_t *size)
{
	size_t		square;
	char		**map;

	square = ft_sqrt(ft_fifolen(fifo) * 4);
	ft_print_fifo(fifo);
	map = ft_init_map(20);
	while (ft_backtrack(fifo, map, square) == 0)
		++square;
	*size = square;
	return (map);
}
