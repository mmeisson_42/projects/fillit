/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_piececmp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 18:08:15 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 18:03:02 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_piece_cmp(char **tab, char *buffer)
{
	size_t		i;

	i = 0;
	ft_putendl("......................................\n");
	while (i < 4)
	{
		printf("'%.4s' VS '%s'\n", buffer + (i * 5), tab[i]);
		if (ft_strncmp(tab[i], buffer + (i * 5), 4))
		{
	ft_putendl("......................................\n");
			return (1);
		}
		i++;
	}
	ft_putendl("......................................\n");
	return (0);
}

t_fifo		*ft_fifo_cmp(t_fifo *fifo, char *buffer)
{
	while (fifo)
	{
		if (!(ft_piece_cmp(fifo->piece->piece, buffer)))
		{
			exit(0);
			return (fifo);
		}
		fifo = fifo->next;
	}
	return (NULL);
}
