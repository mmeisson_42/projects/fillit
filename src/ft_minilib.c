/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minilib.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 13:54:21 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 16:46:42 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		**ft_init_map(size_t size)
{
	char		**tab;
	size_t		i;

	i = 0;
	if (!(tab = malloc(sizeof(char*) * size)))
		ft_exit();
	while (i < size)
	{
		if (!(tab[i] = malloc(sizeof(char) * size)))
			ft_exit();
		ft_memset(tab[i], '.', sizeof(char) * size);
		tab[i][size] = '\0';
		i++;
	}
	return (tab);
}

int			ft_sqrt(int nb)
{
	int		i;

	i = 0;
	if (nb < 1)
		return (0);
	while (i * i < nb)
		i++;
	return (i);
}

size_t		ft_fifolen(t_fifo *fifo)
{
	size_t		i;

	i = 0;
	while (fifo)
	{
		fifo = fifo->next;
		i++;
	}
	return (i);
}
