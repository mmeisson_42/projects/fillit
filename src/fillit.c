/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 13:43:39 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 18:06:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int ac, char **av)
{
	t_fifo		*fifo;
	t_fifo		*tmp;
	char		**map;
	size_t		size;

	if (ac != 2)
		ft_error();
	fifo = NULL;
	size = 0;
	ft_getfile(av[1], &fifo);
	tmp = fifo;
	while (tmp)
	{
		ft_move_piece(tmp->piece->piece);
		tmp = tmp->next;
	}
	map = ft_resolve(fifo, &size);
	ft_display_map(map, size);
}
