/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_pieces.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 19:50:42 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/26 18:01:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_check_form(char *buffer)
{
	size_t		i;
	size_t		j;

	i = 0;
	j = 0;
	while (i < 20)
	{
		if ((i + 1) % 5 == 0 && buffer[i] != '\n')
			return (3);
		if ((i + 1) % 5 && buffer[i] != '.' && buffer[i] != '#')
			return (2);
		if ((i + 1 % 5) && buffer[i] == '#')
			++j;
		++i;
	}
	return (j == 4) ? 0 : 1;
}

void			ft_check_piece(char *buffer)
{
	int		ret;
	
	ret = ft_check_form(buffer);
	if (ret != 0)
	{
		printf("erreur de check form, code %d\n", ret);
		ft_error();
	}
}
